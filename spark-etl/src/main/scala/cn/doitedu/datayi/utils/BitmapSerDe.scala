package cn.doitedu.datayi.utils

import org.roaringbitmap.RoaringBitmap

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, DataInputStream, DataOutputStream}

object BitmapSerDe {

  def ser(bitmap:RoaringBitmap):Array[Byte] = {
    val byteStream = new ByteArrayOutputStream()
    val dataSteam = new DataOutputStream(byteStream)
    bitmap.serialize(dataSteam)

    byteStream.toByteArray
  }


  def de(bytes:Array[Byte]):RoaringBitmap = {

    val bm: RoaringBitmap = RoaringBitmap.bitmapOf()

    // 先将序列化字节数组，反序列化成bitmap对象
    val byteStream = new ByteArrayInputStream(bytes)
    val dataStream = new DataInputStream(byteStream)
    bm.deserialize(dataStream)

    bm
  }

}
