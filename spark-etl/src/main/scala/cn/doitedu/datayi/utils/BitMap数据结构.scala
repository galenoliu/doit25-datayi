package cn.doitedu.datayi.utils

import org.roaringbitmap.RoaringBitmap

object BitMap数据结构 {

  def main(args: Array[String]): Unit = {

    // 1, 3  ,5  ,9
    // 1000101010

    val bitmap1: RoaringBitmap = RoaringBitmap.bitmapOf(1, 3, 5, 9,5,6,9,3,3,3)
    val bitmap2: RoaringBitmap = RoaringBitmap.bitmapOf(2, 3, 4, 5, 5,3)

    // 求并集
    // bitmap1.or(bitmap2)

    // 求交集
    bitmap1.and(bitmap2)


    val cardinality: Int = bitmap1.getCardinality
    println(cardinality)



  }
}
