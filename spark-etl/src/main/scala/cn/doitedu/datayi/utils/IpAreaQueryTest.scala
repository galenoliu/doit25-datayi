package cn.doitedu.datayi.utils

import org.lionsoul.ip2region.{DataBlock, DbConfig, DbSearcher}

object IpAreaQueryTest {

  def main(args: Array[String]): Unit = {

    val searcher = new DbSearcher(new DbConfig(), "data/ip2region/ip2region.db")

    val block1: DataBlock = searcher.memorySearch("222.64.110.208")
    val block2: DataBlock = searcher.memorySearch("222.64.110.108")
    val block3: DataBlock = searcher.memorySearch("192.168.1.101")
    val block4: DataBlock = searcher.memorySearch("192.168.1.255")
    val block5: DataBlock = searcher.memorySearch("255.255.255.255")
    val block6: DataBlock = searcher.memorySearch("200.200.100.8")

    /**
     * 中国|0|上海|上海|电信
     *  0|0|0|内网IP|内网IP
     */
    println(block1.getRegion)
    println(block2.getRegion)
    println(block3.getRegion)
    println(block4.getRegion)
    println(block5.getRegion)
    println(block6.getRegion)

  }

}
