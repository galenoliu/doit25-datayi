package cn.doitedu.datayi.etl

case class Applog(
                   var account             : String                 ,
                   val appid               : String                 ,
                   val appversion          : String                 ,
                   val carrier             : String                 ,
                   val device_id           : String                 ,
                   val devicetype          : String                 ,
                   val eventid             : String                 , // pageview 事件
                   val ip                  : String                 ,
                   val latitude            : Double                 ,
                   val longitude           : Double                 ,
                   val nettype             : String                 ,
                   val osname              : String                 ,
                   val osversion           : String                 ,
                   val properties          : Map[String,String]     ,
                   val releasechannel      : String                 ,
                   val resolution          : String                 ,
                   val sessionid           : String                 ,
                   val timestamp           : Long                   ,
                   var splitedSessionId    : String = ""            ,
                   var prvoince            : String = ""            ,
                   var city                : String = ""            ,
                   var region              : String = ""            ,
                   var guid                : Long = -1
                 )
