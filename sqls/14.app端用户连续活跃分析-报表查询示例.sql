-- 用户活跃报表分析举例

-- 最近一个月内，连续活跃天数在 [1,10),[10,20),[20+ )分别有多少用户
WITH tmp as (
    SELECT

        guid,
        MAX(if(rng_end='9999-12-31','2021-09-18',rng_end)-if(datediff('2021-09-18',rng_start)>30,date_sub('2021-09-18',29),rng_start)) as max_ctn_days


    FROM dws.mall_app_user_ctn_atv
    WHERE dt='2021-09-18' AND  datediff('2021-09-18',rng_end)<=30
    GROUP BY guid
)
--g02,8
--g03,15
--g04,4
--g01,12


SELECT
    cnt_level,
    count(1) as level_user_cnt
FROM  (
          SELECT
              guid,
              case
                  when max_ctn_days<10 then 1
                  when max_ctn_days>=10 and max_ctn_days<20 then 2
                  else 3
                  end as ctn_level
          FROM  tmp
      ) o1
GROUP BY cnt_level


-- 查询  [2021-09-01,2021-09-10]期间，有连续活跃超4天的人
SELECT

    guid

FROM dws.mall_app_user_ctn_atv
WHERE dt='2021-09-18' AND  rng_end>='2021-09-01'  AND rng_start<='2021-09-10'
GROUP BY guid
HAVING MAX(if(rng_end>'2021-09-10','2021-09-10',rng_end) - if(rng_start<'2021-09-01','2021-09-01',rng_start)) >= 3