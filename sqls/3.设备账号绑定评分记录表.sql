DROP TABLE IF EXISTS dws.mall_app_dvc_bds;
CREATE TABLE dws.mall_app_dvc_bds(
  device_id  STRING,    -- 设备
  account    STRING,    -- 账号
  last_login BIGINT,    -- 账号最近登录时间
  bind_score FLOAT      -- 评分

)
PARTITIONED BY (DT STRING)
STORED AS ORC
;


-- 计算sql 代码实现 --


/*
 * 1. 对当天日志进行评分聚合
     计算： 每一个 “设备+账号”组合的登录次数、最后登录时间
*/
WITH cur AS(
SELECT
  deviceid                         as device_id,
  if(account='',null,account)      as account,
  count(distinct sessionid) * 10   as bind_score,
  max(`timestamp`)                 as last_login
FROM ods.mall_app_action_log
WHERE dt='2021-09-18'
GROUP BY deviceid,if(account='',null,account)
)

/*
 * 2. 去评分记录表的T-1分区数据
     
*/
,his AS(
SELECT
  device_id,
  account,
  bind_score,
  last_login
FROM dws.mall_app_dvc_bds 
WHERE dt='2021-09-17'

)


/*
 * 3. 对上面两个表做full join计算
      deviceid:  哪边有取哪边
      account:  哪边有取哪边
      last_login: cur有一定取cur，否则取his
      bind_score:   
           cur有account,    his有account， 分数相加
           cur没有account,  his有account, his分数衰减（如果历史数据score为null，则默认0）
           cur有account,   his没有account, 取cur的分数
           
     
*/
INSERT INTO TABLE dws.mall_app_dvc_bds PARTITION(dt = '2021-09-18')
SELECT
  nvl(cur.device_id,his.device_id) as device_id,
  nvl(cur.account,his.account) as account,
  nvl(cur.last_login,his.last_login) as last_login,
  CASE
    WHEN cur.account is not null and his.account is not null then cur.bind_score + his.bind_score
    WHEN cur.account is not null and his.account is null then cur.bind_score
    else nvl(his.bind_score*0.5,0)
  END as bind_score
FROM  cur FULL JOIN his
ON cur.device_id = his.device_id AND cur.account = his.account


