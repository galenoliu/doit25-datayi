-- 报表
-- 各省、市、区的uv数，pv数，会话数，访问总时长，跳出会话数


SELECT
    province,
    city,
    region,
    uv_cnt,
    pv_cnt,
    ses_cnt,
    visit_time,
    jmp_cnt
FROM dws.mall_app_tfc_cube
WHERE dt='2021-09-18' AND province is not null AND city is not null AND region is not NULL
  AND COALESCE(
        appid,
        appversion,
        releasechannel,
        carrier,
        nettype,
        device_type,
        osname,
        osversion,
        resolution,
        enter_page,
        exit_page
    ) is null
;

-- 今天各种手机型号的 uv数，pv数，会话数，访问总时长，跳出会话数
SELECT
    device_type,
    uv_cnt,
    pv_cnt,
    ses_cnt,
    visit_time,
    jmp_cnt
FROM dws.mall_app_tfc_cube
WHERE dt='2021-09-18' AND device_type is not NULL
  AND COALESCE(
        appid,
        appversion,
        releasechannel,
        carrier,
        nettype,
        osname,
        osversion,
        resolution,
        enter_page,
        province,
        city,
        region,
        exit_page
    ) is null
;



-- 今天各省中，uv数最高的前3个城市及uv数（如果uv相同，按pv比）
with tmp as (
    SELECT
        province,
        city,
        uv_cnt,
        pv_cnt,
        row_number() over(partition by province order by uv_cnt desc,pv_cnt desc)  as rn
    FROM dws.mall_app_tfc_cube
    WHERE dt='2021-09-18' AND province is not null AND city is not null
      AND COALESCE(
            appid,
            appversion,
            releasechannel,
            carrier,
            nettype,
            device_type,
            osname,
            region,
            osversion,
            resolution,
            enter_page,
            exit_page
        ) is null
)
select
    province,
    city,
    uv_cnt,
    pv_cnt,
    rn
from  tmp
where rn<=3
