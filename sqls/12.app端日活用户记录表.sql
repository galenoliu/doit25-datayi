-- 日活表
DROP TABLE IF EXISTS dws.mall_app_dau;
CREATE TABLE dws.mall_app_dau(
                                 guid  bigint,
                                 isnew int,
                                 first_dt string
)
    PARTITIONED BY (dt string)
    STORED AS ORC
    TBLPROPERTIES('orc.compress'='SNAPPY')
;



-- 计算sql
with tmp1 as (
    SELECT
        guid
    FROM dwd.mall_app_evt_dtl
    WHERE dt='2021-09-18'
    GROUP BY guid
)
   ,tmp2 as (
    SELECT
        guid,
        first_dt
    FROM  dws.mall_app_user_ctn_atv
    WHERE dt='2021-09-17'
    GROUP BY guid,first_dt
)
INSERT INTO TABLE dws.mall_app_dau PARTITION(dt='2021-09-18')
SELECT
    tmp1.guid,
    if(tmp2.guid is not null,0,1) as isnew,
    nvl(tmp2.first_dt,'2021-09-18') as first_dt

FROM  tmp1 left join tmp2 on tmp1.guid=tmp2.guid