CREATE TABLE ods.mall_wx_action_log(
   account        STRING,                                
   carrier        STRING,                                
   deviceid       STRING,                                
   devicetype     STRING,                                
   eventid        STRING,                                
   ip             STRING,                                
   latitude       DOUBLE,
   longitude      DOUBLE,
   nettype        STRING,                                
   openid         STRING,                                
   osname         STRING,                                
   osversion      STRING,                                
   properties     MAP<STRING,STRING>,
   resolution     STRING,                                
   sessionid      STRING,                                
   `timestamp`      BIGINT
)
PARTITIONED BY (DT STRING)
ROW FORMAT SERDE
    'org.apache.hive.hcatalog.data.JsonSerDe'
STORED AS TEXTFILE
TBLPROPERTIES(
  'EXTERNAL'='true'
);
