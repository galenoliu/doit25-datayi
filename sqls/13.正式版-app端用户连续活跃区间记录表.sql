-- 连续活跃区间记录表建表
create table dws.mall_app_user_ctn_atv(
     guid bigint,
     first_dt string,
     rng_start string,
     rng_end string
)
    PARTITIONED BY ( dt STRING)
    STORED AS ORC
    TBLPROPERTIES('orc.compress'='SNAPPY')
;


-- 计算sql

INSERT INTO TABLE dws.mall_app_user_ctn_atv PARTITION(dt='2021-09-18')
--part1 : 所有的已封闭区间数据
SELECT
    guid,first_dt,rng_start,rng_end
FROM dws.mall_app_user_ctn_atv
WHERE dt='2021-09-17' AND rng_end != '9999-12-31'


UNION ALL

--part2 ：所有的开放区间  FULL JOIN 日活
-- 2,2021-07-05,2021-08-05,9999-12-31
--                                     3,0,2021-08-01
-- 4,2021-07-05,2021-08-05,9999-12-31  4,0,2021-07-05
--                                     5,1,2021-08-10
SELECT
    nvl(o1.guid,o2.guid)           as guid,
    nvl(o1.first_dt,o2.first_dt)   as first_dt,
    nvl(o1.rng_start,'2021-09-18') as rng_start,
    if(o1.rng_end is not null and o2.guid is null,date_sub('2021-09-18',1),'9999-12-31') as rng_end
FROM
    (
        SELECT
            guid,first_dt,rng_start,rng_end
        FROM dws.mall_app_user_ctn_atv
        WHERE dt='2021-09-17' AND rng_end = '9999-12-31'
    ) o1

        FULL JOIN

    (
        SELECT
            guid,isnew,first_dt
        FROM
            dws.mall_app_dau
        WHERE dt='2021-09-18'
    ) o2

    ON o1.guid = o2.guid
;