-- 2021-08-09 连续活跃区间记录表
/*
guid,first_dt,rng_start,rng_end
1,2021-07-01,2021-07-01,2021-07-08
1,2021-07-01,2021-08-01,2021-08-02
2,2021-07-05,2021-07-05,2021-07-15
2,2021-07-05,2021-08-05,9999-12-31
3,2021-08-01,2021-08-01,2021-08-05
3,2021-08-01,2021-08-08,2021-08-08
4,2021-07-05,2021-07-05,2021-07-15
4,2021-07-05,2021-08-05,9999-12-31
*/
create table test.app_user_ctn_atv(
                                      guid bigint,
                                      first_dt string,
                                      rng_start string,
                                      rng_end string
)
    partitioned by ( dt string)
    row format delimited fields terminated by ',';


load data local inpath '/root/rng.csv' into table test.app_user_ctn_atv partition(dt='2021-08-09');



-- 2021-08-10  日活表
/*
guid,isnew,first_dt
3,0,2021-08-01
4,0,2021-07-05
5,1,2021-08-10
*/

create table test.app_user_dau(
                                  guid bigint,
                                  isnew int,
                                  first_dt string
)
    partitioned by (dt string)
    row format delimited fields terminated by ',';


load data local inpath '/root/dau.csv' into table test.app_user_dau partition(dt='2021-08-10');



-- 计算sql

--part1 : 所有的已封闭区间数据
SELECT
    guid,first_dt,rng_start,rng_end
FROM TEST.app_user_ctn_atv
WHERE dt='2021-08-09' AND rng_end != '9999-12-31'


UNION ALL

--part2 ：所有的开放区间  FULL JOIN 日活
-- 2,2021-07-05,2021-08-05,9999-12-31
--                                     3,0,2021-08-01
-- 4,2021-07-05,2021-08-05,9999-12-31  4,0,2021-07-05
--                                     5,1,2021-08-10
SELECT
    nvl(o1.guid,o2.guid)           as guid,
    nvl(o1.first_dt,o2.first_dt)   as first_dt,
    nvl(o1.rng_start,'2021-08-10') as rng_start,
    if(o1.rng_end is not null and o2.guid is null,date_sub('2021-08-10',1),'9999-12-31') as rng_end
FROM
    (
        SELECT
            guid,first_dt,rng_start,rng_end
        FROM TEST.app_user_ctn_atv
        WHERE dt='2021-08-09' AND rng_end = '9999-12-31'
    ) o1

        FULL JOIN

    (
        SELECT
            guid,isnew,first_dt
        FROM
            TEST.app_user_dau
        WHERE dt='2021-08-10'
    ) o2

    ON o1.guid = o2.guid




-- 计算结果
-- 2021-08-10 连续活跃区间记录表
/*
 1,2021-07-01,2021-07-01, 2021-07-08
 1,2021-07-01,2021-08-01, 2021-08-02
 2,2021-07-05,2021-07-05, 2021-07-15
 2,2021-07-05,2021-08-05,[2021-08-09]
 3,2021-08-01,2021-08-01,2021-08-05
 3,2021-08-01,2021-08-08,2021-08-08
[3,2021-08-01,2021-08-10,9999-12-31]
 4,2021-07-05,2021-07-05,2021-07-15
 4,2021-07-05,2021-08-05,9999-12-31
 5,2021-08-10,2021-08-10,9999-12-31
*/


