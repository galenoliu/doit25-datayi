-- 首访日：用户连续活跃区间记录表抽取
-- 用户首次下单日期：用户首次下单记录表 UNION 当日首次下单的人-日期

--标签表1（每日新增）
CREATE TABLE dws.mall_profile_firstact(
  first_login_dt    string, -- 首次访问日期
  first_order_dt    string, -- 首次下单日期
  first_comment_dt  string,  --首次评论日期
  dw_dt             string
)
stored as orc
tblproperties('orc.compress'='SNAPPY')
;



-- 用户常在省份（2周更新一次）
-- 用户常在城市（2次周更新一）
-- 用户常用设备型号（2周更新一次）
-- 登录次数（7日、15日、30日）（2周更新一次）
-- 访问时长（7日、15日、30日）（2周更新一次）
-- 访问深度（7日、15日、30日）（2周更新一次）

-- 标签表2（2周更新一次）
DROP TABLE IF EXISTS dws.mall_profile_tag01;
CREATE TABLE dws.mall_profile_tag01(
  guid  bigint
  ,common_province       string
  ,common_city           string
  ,common_device_type    string
  ,login_cnt_7d          int
  ,login_cnt_15d         int
  ,login_cnt_30d         int
  ,acc_timelong_7d       int
  ,acc_timelong_15d      int
  ,acc_timelong_30d      int
  ,acc_depth_7d          int
  ,acc_depth_15d         int
  ,acc_depth_30d         int
  ,dw_dt                string
)
partitioned by (dt string)
stored as orc
tblproperties('orc.compress'='SNAPPY')
;




-- 会话信息用户日聚合表
-- guid,province,city,region,device_type,session_id,访问时长,访问深度
CREATE TABLE dws.mall_app_ses_aggr_u(
  guid   bigint,
  province string,
  city  string,
  region string,
  device_type string,
  session_id string,
  acc_timelong bigint,
  acc_pages    bigint
)
partitioned by (dt string)
stored as orc
tblproperties('orc.compress'='SNAPPY')
;


-- 维护一个日聚合信息；计算sql
INSERT INTO TABLE dws.mall_app_ses_aggr_u PARTITION(dt='2021-09-18')
SELECT
  guid,                               --用户标识
  province,                           -- 省份
  city,                               -- 城市
  region,                             -- 地区
  device_type,                        -- 设备型号
  session_id,                         -- 会话id
  max(ts)-min(ts)  as acc_timelong,   -- 会话时长
  count(1) as acc_pages               -- 会话访问页面数
from dwd.mall_app_evt_dtl
WHERE dt='2021-09-18'
GROUP BY  guid,province,city,region,device_type,session_id




-- 标签计算： 直接从日聚合数据上，直接进行：1周聚合、2周聚合、4周聚合
with tmp1 as (
SELECT
  guid
  ,COUNT(distinct if(datediff('2021-09-18',dt)<7,session_id,null))  AS login_cnt_7d         -- 7日登录次数
  ,COUNT(distinct if(datediff('2021-09-18',dt)<15,session_id,null))  AS login_cnt_15d       -- 15日登录次数
  ,COUNT(distinct if(datediff('2021-09-18',dt)<30,session_id,null))  AS login_cnt_30d       -- 30日登录次数
  ,SUM(if(datediff('2021-09-18',dt)<7,acc_timelong,0)) AS acc_timelong_7d                   -- 7日访问时长
  ,SUM(if(datediff('2021-09-18',dt)<15,acc_timelong,0)) AS acc_timelong_15d     -- 15日访问时长
  ,SUM(if(datediff('2021-09-18',dt)<30,acc_timelong,0)) AS acc_timelong_30d     -- 30日访问时长
  ,sum(if(datediff('2021-09-18',dt)<7,acc_pages,0))/count(distinct if(datediff('2021-09-18',dt)<7,session_id,null))   AS acc_depth_7d          -- 7日访问平均深度
  ,sum(if(datediff('2021-09-18',dt)<15,acc_pages,0))/count(distinct if(datediff('2021-09-18',dt)<15,session_id,null))   AS acc_depth_15d       -- 15日访问平均深度
  ,sum(if(datediff('2021-09-18',dt)<30,acc_pages,0))/count(distinct if(datediff('2021-09-18',dt)<30,session_id,null))   AS acc_depth_30d       -- 30日访问平均深度

FROM  dws.mall_app_ses_aggr_u
WHERE dt<='2021-09-18' and dt>=date_sub('2021-09-18',29)
GROUP BY guid

),

  -- AS common_province     -- 常在省份
  -- AS common_city         -- 常在城市
  -- AS common_device_type  -- 常用设备型号


tmp2 as (
SELECT
  guid,
  province as common_province
FROM
(
SELECT
  guid,
  province,
  row_number()  over(PARTITION BY guid order by count(distinct dt) desc ) as rn
FROM  dws.mall_app_ses_aggr_u
WHERE dt<='2021-09-18' and dt>=date_sub('2021-09-18',14)
GROUP BY guid,province
) o
WHERE rn = 1
),

tmp3 as (
SELECT
  guid,
  common_city
FROM
(
SELECT
  guid,
  concat_ws(':',province,city) as common_city,
  row_number()  over(PARTITION BY guid order by count(distinct dt) desc ) as rn
FROM  dws.mall_app_ses_aggr_u
WHERE dt<='2021-09-18' and dt>=date_sub('2021-09-18',14)
GROUP BY guid,concat_ws(':',province,city)
) o
WHERE rn = 1
),

tmp4 as (
SELECT
  guid,
  device_type as common_device_type
FROM
(
SELECT
  guid,
  device_type,
  row_number()  over(PARTITION BY guid order by count(distinct dt) desc ) as rn
FROM  dws.mall_app_ses_aggr_u
WHERE dt<='2021-09-18' and dt>=date_sub('2021-09-18',14)
GROUP BY guid,device_type
) o
WHERE rn = 1

)

INSERT INTO dws.mall_profile_tag01 PARTITION (dt ='2021-09-18')
SELECT tmp1.guid
  ,tmp2.common_province     
  ,tmp3.common_city         
  ,tmp4.common_device_type  
  ,tmp1.login_cnt_7d        
  ,tmp1.login_cnt_15d       
  ,tmp1.login_cnt_30d       
  ,tmp1.acc_timelong_7d     
  ,tmp1.acc_timelong_15d    
  ,tmp1.acc_timelong_30d    
  ,tmp1.acc_depth_7d        
  ,tmp1.acc_depth_15d       
  ,tmp1.acc_depth_30d   
  ,'2021-09-18' as dw_dt  
FROM   tmp1  left join tmp2  on tmp1.guid = tmp2.guid
             left join tmp3  on tmp1.guid = tmp3.guid
             left join tmp4  on tmp1.guid = tmp4.guid

