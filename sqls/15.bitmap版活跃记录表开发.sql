-- 活跃记录bitmap表
CREATE TABLE dws.mall_app_ctn_bitmap(
    guid          bigint,
    atv_bitmap    int
)
PARTITIONED BY (dt string)
stored as orc;


-- 2021-09-26
-- 初始化整个bitmap活跃记录表
-- 日活表:
/*
1,2021-09-26
2,2021-09-26
4,2021-09-26
1,2021-09-25
4,2021-09-25
1,2021-09-24
2,2021-09-24
2,2021-09-23
4,2021-09-23
2,2021-09-22
4,2021-09-22
1,2021-09-20
2,2021-09-20
4,2021-09-20
1,2021-09-18
4,2021-09-18
4,2021-09-02
1,2021-09-01
2,2021-09-01
*/
create table dws.test_mall_app_dau(
   guid   bigint,
   dt     string
)
row format delimited fields terminated by ',';
load data local inpath '/root/dau.txt' into table dws.test_mall_app_dau;




-- 计算初始化bitmap活跃状态表的核心逻辑：  计算日-活跃日作为2幂次，然后按用户累加
-- g01,2021-09-26      2021-09-26     0    2^0
-- g01,2021-09-25      2021-09-26     1    2^1      => 7
-- g01,2021-09-24      2021-09-26     2    2^2
--
-- g02,2021-09-26      2021-09-26     0    2^0
-- g02,2021-09-24      2021-09-26     2    2^2      => 13
-- g02,2021-09-23      2021-09-26     3    2^3
--
-- g04,2021-09-26      2021-09-26     0    2^0
-- g04,2021-09-25      2021-09-26     1    2^1      => 11
-- g04,2021-09-23      2021-09-26     3    2^3

INSERT INTO TABLE dws.mall_app_ctn_bitmap PARTITION(dt='2021-09-26')
SELECT
    guid,
    sum(cast(pow(2,datediff('2021-09-26',dt)) as int)) as atv_bitmap
from dws.test_mall_app_dau
where datediff('2021-09-26',dt)<=30
group by guid
;



-- bitmap表初始化结果：
/*
+-------+-------------+
| guid  | atv_bitmap  |
+-------+-------------+
| 1     | 33554759    |
| 2     | 33554525    |
| 4     | 16777563    |
+-------+-------------+
*/
-- g01, 0000000000000000000000000000111
-- g02, 0000000000000000000000000001101
-- g04, 0000000000000000000000000001011



-- 时间推移到了 2021-09-27 号，需要更新：活跃记录bitmap表
-- 2021-09-27 日活数据
/*
1,2021-09-27
2,2021-09-27
5,2021-09-27
*/
load data local inpath '/root/dau2.txt' into table dws.test_mall_app_dau;



-- 更新逻辑sql：
--  (原来的值*2 + (今天活跃?1:0)) & 整数最大值
--    |1000000000000000000000000001011
--   1|000000000000000000000000001011?   左移补位
INSERT INTO TABLE dws.mall_app_ctn_bitmap PARTITION( dt='2021-09-27')
SELECT
    nvl(bm.guid,dau.guid) as guid,
    case
        when bm.guid is not null and dau.guid is not null  then   (atv_bitmap * 2  + 1) & 2147483647
        when bm.guid is not null and dau.guid is null  then   (atv_bitmap * 2) & 2147483647
        else 1
    END as atv_bitmap

FROM

    (SELECT
         guid,
         atv_bitmap
     FROM  dws.mall_app_ctn_bitmap
     WHERE dt='2021-09-26') bm

        FULL JOIN

    (SELECT
         guid
     FROM  dws.test_mall_app_dau
     WHERE dt='2021-09-27') dau

    ON bm.guid = dau.guid
