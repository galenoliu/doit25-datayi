-- 会话信息聚合表
DROP TABLE IF EXISTS dws.mall_app_ses_agr;
CREATE TABLE dws.mall_app_ses_agr(
   guid        bigint,
   session_id  string,
   start_time  bigint,
   end_time    bigint,
   pv_cnt      bigint,
   enter_page  string,
   exit_page   string,
   is_jump     int
)
PARTITIONED BY (dt  string)
STORED AS ORC
TBLPROPERTIES('orc.compress'='SNAPPY')
;



-- 计算sql

--part1: 会话起始、结束时间，访问页数
with p1 as (
SELECT
   guid
  ,session_id
  ,min(ts) as start_time
  ,max(ts) as end_time
  ,count(if(event_id in ('pageView','productView'),1,null)) as pv_cnt
FROM dwd.mall_app_evt_dtl 
WHERE dt='2021-09-18'
GROUP BY guid,session_id
)

--part2:入口页,退出页
, p2 as (
SELECT
  guid
  ,session_id
  ,split(min(concat_ws('_',ts || '',properties['pageId'])),'_')[1] as enter_page
  ,split(max(concat_ws('_',ts || '',properties['pageId'])),'_')[1] as exit_page
FROM dwd.mall_app_evt_dtl 
WHERE dt='2021-09-18' AND event_id in ('pageView','productView')
GROUP BY guid,session_id
)


-- 整合结果
INSERT INTO TABLE dws.mall_app_ses_agr PARTITION(dt='2021-09-18')
SELECT
  p1.guid
  ,p1.session_id
  ,p1.start_time
  ,p1.end_time
  ,p1.pv_cnt
  ,p2.enter_page
  ,p2.exit_page
  ,if(p1.pv_cnt<2,1,0) AS is_jump
FROM p1 LEFT JOIN p2 
ON p1.guid=p2.guid AND p1.session_id = p2.session_id
