###############   全量导入示例  ###################
bin/sqoop import \
--connect jdbc:mysql://doit01:3306/realtimedw \
--username root \
--password root \
--table dim_pginfo \
--target-dir /sqoopdata/dim_pginfo  \
--as-textfile   \
--fields-terminated-by '\001' \
--delete-target-dir \
--compress       \
--compression-codec gzip   \
--split-by id \
--null-non-string   '\\N'   \
--null-string  '\\N'   \
-m 2

###############   增量导入HDFS示例  ###################
# 从mysql导入增量数据到hdfs
# 模拟前一日2021-09-28第一次导入（全量）
bin/sqoop import \
--connect jdbc:mysql://192.168.77.2:3306/realtimedw \
--username root \
--password 123456 \
--target-dir /sqoopdata/oms_order/2021-09-28  \
--as-textfile   \
--fields-terminated-by '\001' \
--delete-target-dir \
--split-by id \
--null-non-string   '\\N'   \
--null-string  '\\N'   \
--query 'select * from oms_order where modify_time<"2021-09-29 00:00:00" and  $CONDITIONS '  \
-m 2

# 模拟后一日2021-09-29增量导入
bin/sqoop import \
--connect jdbc:mysql://192.168.77.2:3306/realtimedw \
--username root \
--password 123456 \
--target-dir /sqoopdata/oms_order/2021-09-29  \
--as-textfile   \
--fields-terminated-by '\001' \
--delete-target-dir \
--split-by id \
--null-non-string   '\\N'   \
--null-string  '\\N'   \
--query 'select * from oms_order where modify_time>="2021-09-29 00:00:00"  and  modify_time<"2021-09-30 00:00:00"              and  $CONDITIONS '  \
-m 1



###############  导入hive示例  ###################
# 从mysql导入增量数据到hdfs
# 模拟前一日2021-09-28第一次导入（全量）
bin/sqoop import \
--connect jdbc:mysql://192.168.77.2:3306/realtimedw \
--username root \
--password 123456 \
--query 'select * from oms_order where modify_time<"2021-09-29 00:00:00" and  $CONDITIONS ' \
--hive-import \
--hive-partition-key dt  \
--hive-partition-value '2021-09-28' \
--hive-table ods.oms_order_incr \
--target-dir /sqoopdata/hiveimport/oms_order/2021-09-28  \
--delete-target-dir \
--as-textfile \
--fields-terminated-by '\001' \
--compress   \
--compression-codec gzip \
--null-string '\\N' \
--null-non-string '\\N' \
--hive-overwrite \
--split-by id \
-m 2
# --hive-database ods 

# 模拟前一日2021-09-29的增量数据
bin/sqoop import \
--connect jdbc:mysql://192.168.77.2:3306/realtimedw \
--username root \
--password 123456 \
--query 'select * from oms_order where modify_time>="2021-09-29 00:00:00"  and  modify_time<"2021-09-30 00:00:00" and  $CONDITIONS ' \
--hive-import \
--hive-partition-key dt  \
--hive-partition-value '2021-09-29' \
--hive-table ods.oms_order_incr \
--target-dir /sqoopdata/hiveimport/oms_order/2021-09-29  \
--delete-target-dir \
--as-textfile \
--fields-terminated-by '\001' \
--compress   \
--compression-codec gzip \
--null-string '\\N' \
--null-non-string '\\N' \
--hive-overwrite \
--split-by id \
-m 2












