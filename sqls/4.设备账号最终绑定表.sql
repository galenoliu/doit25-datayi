create table dws.mall_app_dev_bind_f(
  device_id  string,
  account    string
)
partitioned by (dt string)
stored as orc
tblproperties(
 "orc.compress"="snappy"
)
;


-- 计算设备账号最终绑定表sql
-- 源表： dws.mall_app_dev_bind
-- 目标： dws.mall_app_dev_bind_f

-- 源表数据示例
+---------------+------------+----------------+---------------+-------------+
|  t.device_id  | t.account  |  t.last_login  | t.bind_score  |    t.dt     |
+---------------+------------+----------------+---------------+-------------+
| 00Xf51XPJXHM  | B7QH       | 1631943864282  | 60.0          | 2021-09-18  |
| 0BGk4WsYJTcn  | NULL       | 1631944775477  | 0.0           | 2021-09-18  |
| 0BHIlK0i9t09  | NULL       | 1631943886911  | 0.0           | 2021-09-18  |
| 0BpbOuKsuIAh  | DxL36Dom   | 1631944249274  | 10.0          | 2021-09-18  |
| 0EGpNyTBv1IH  | B7QH       | 1631943987169  | 20.0          | 2021-09-18  |
+---------------+------------+----------------+---------------+-------------+
INSERT INTO TABLE dws.mall_app_dev_bind_f PARTITION(dt='2021-09-18')
SELECT
 device_id,
 account
FROM
(
SELECT
  device_id,
  account,
  row_number() over(partition by device_id order by bind_score desc,last_login desc ) as rn 
FROM dws.mall_app_dev_bind
WHERE dt='2021-09-18'
) o 
WHERE rn=1
;

