CREATE TABLE ods.mall_app_action_log (
account                 STRING,                
appId                   STRING,                
appVersion              STRING,                
carrier                 STRING,                
deviceId                STRING,                
deviceType              STRING,                
eventId                 STRING,                
ip                      STRING,                
latitude                DOUBLE,
longitude               DOUBLE,
netType                 STRING,                
osName                  STRING,                
osVersion               STRING,                
properties              MAP<STRING,STRING>,
releaseChannel          STRING,           
resolution              STRING,           
sessionId               STRING,           
`timeStamp`             BIGINT
)
PARTITIONED BY (dt string)
ROW FORMAT  SERDE   
    'org.apache.hive.hcatalog.data.JsonSerDe'
STORED AS TEXTFILE
TBLPROPERTIES(
  'EXTERNAL'='true'
);

