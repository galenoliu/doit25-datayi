-- 报表查询
-- 需求1：求指定日期范围内有过活跃的人
-- 需求2：求指定日期范围内连续活跃的人
-- 需求3：求指定日期范围内每个人的活跃天数
-- 需求4：求最近一月内，每个人的最大连续活跃天数


-- 源表： dws.mall_app_ctn_bitmap 活跃记录bitmap表  2021-09-27
/*
+-------+-------------+
| guid  | atv_bitmap  |
+-------+-------------+
| 1     | 67109519    |
| 2     | 67109051    |
| 4     | 33555126    |
| 5     | 1           |
+-------+-------------+
*/

-- 需求1：求指定日期范围内[2021-09-18,2021-09-26]有过活跃的人
SELECT
    guid
FROM dws.mall_app_ctn_bitmap
WHERE dt='2021-09-27' AND
-- 将bitmap整数转成二进制字符串，然后根据脚标和日期的对应规律截子串，然后替换掉所有的0位空串，看看剩下的长度是否大于0
length(replace(substr(lpad(bin(atv_bitmap),32,'0'), 32-datediff('2021-09-27','2021-09-18'),datediff('2021-09-26','2021-09-18')+1 ),'0',''))>0



-- 需求2：求指定日期范围内[2021-09-24,2021-09-26]连续活跃的人
SELECT
    guid
FROM dws.mall_app_ctn_bitmap
WHERE dt='2021-09-27' AND
-- 将bitmap整数转成二进制字符串，然后根据脚标和日期的对应规律截子串，然后替换掉所有的0位空串，看看剩下的长度是否大于0
length(replace(substr(lpad(bin(atv_bitmap),32,'0'), 32-datediff('2021-09-27','2021-09-24'),datediff('2021-09-26','2021-09-24')+1 ),'1','')) = 0
;

-- 需求3：求指定日期范围内[2021-09-24,2021-09-26]每个人的活跃天数
SELECT
    guid,
    length(replace(substr(lpad(bin(atv_bitmap),32,'0'), 32-datediff('2021-09-27','2021-09-24'),datediff('2021-09-26','2021-09-24')+1 ),'0','')) as atv_days
FROM dws.mall_app_ctn_bitmap
WHERE dt='2021-09-27';

-- 需求4：求最近一月内，每个人的最大连续活跃天数
-- 步骤1：将bitmap整数转成字符串，然后按照0+切
SELECT
    guid,
    split(lpad(bin(atv_bitmap),32,'0') ,'[0]+')  -- 000001111101001011110111111111110000
FROM dws.mall_app_ctn_bitmap
WHERE dt='2021-09-27';
/*
+-------+--------------------------------+
| guid  |              _c1               |
+-------+--------------------------------+
| 1     | ["","1","1","1","1111"]        |
| 2     | ["","1","1","111","11"]        |
| 4     | ["","1","1","1","11","11",""]  |
| 5     | ["","1"]                       |
+-------+--------------------------------+
*/

-- 步骤2：把步骤1的结果炸裂
with tmp as (
    SELECT
        guid,
        atv_bitmap
    FROM dws.mall_app_ctn_bitmap
    WHERE dt='2021-09-27'
)
SELECT
    guid,
    max(length(str)) as max_atv_days
FROM tmp
         LATERAL VIEW explode(split(lpad(bin(atv_bitmap),32,'0') ,'[0]+')) o as str
GROUP BY guid
;







