-- 归因分析数据汇总表建表
drop table if exists dws.mall_app_evt_attr;
create table dws.mall_app_evt_attr
(
    guid         bigint,
    dest_ev      string,
    strategy     string,
    attr_ev      string,
    attr_factor  float,
    window_start string,
    window_end   string
)
    partitioned by (dt string)
    stored as orc
    tblproperties ('orc.compress' = 'SNAPPY')
;


-- 测试用的事件明细表
/*
 +---------+------------+------------------------+-------+-------------+
| t.guid  | t.eventid  |      t.properties      | t.ts  |    t.dt     |
+---------+------------+------------------------+-------+-------------+
| 1       | e1         | {"p1":"v1","p3":"v3"}  | 1     | 2021-09-26  |
| 1       | e2         | {"p1":"v2","p2":"v3"}  | 2     | 2021-09-26  |
| 1       | e3         | {"p2":"v5","p3":"v3"}  | 3     | 2021-09-26  |
| 1       | e3         | {"p2":"v4","p3":"v3"}  | 4     | 2021-09-26  |
| 1       | e4         | {"p6":"v2","p2":"v4"}  | 5     | 2021-09-26  |
| 1       | e5         | {"p8":"v8","p3":"v3"}  | 6     | 2021-09-26  |
| 1       | e2         | {"p2":"v4","p3":"v3"}  | 7     | 2021-09-26  |
| 1       | e5         | {"p6":"v2","p2":"v4"}  | 8     | 2021-09-26  |
| 1       | e4         | {"p8":"v8","p3":"v3"}  | 9     | 2021-09-26  |
| 2       | e3         | {"p2":"v5","p3":"v3"}  | 1     | 2021-09-26  |
| 2       | e4         | {"p1":"v1","p3":"v3"}  | 2     | 2021-09-26  |
| 2       | e1         | {"p1":"v1","p3":"v3"}  | 3     | 2021-09-26  |
| 2       | e5         | {"p8":"v8","p3":"v3"}  | 4     | 2021-09-26  |
| 3       | e3         | {"p2":"v5","p3":"v3"}  | 1     | 2021-09-26  |
| 3       | e4         | {"p1":"v1","p3":"v3"}  | 2     | 2021-09-26  |
| 3       | e1         | {"p1":"v1","p3":"v3"}  | 3     | 2021-09-26  |
| 3       | e5         | {"p8":"v8","p3":"v3"}  | 4     | 2021-09-26  |
| 3       | e3         | {"p2":"v5","p3":"v3"}  | 5     | 2021-09-26  |
| 3       | e4         | {"p2":"v5","p3":"v3"}  | 6     | 2021-09-26  |
+---------+------------+------------------------+-------+-------------+
 */

-- 归因需求
-- 目标事件： e4
-- 待归因事件： e1  e2  e3
-- 归因策略： 首次触点归因

-- 计算sql
-- 1。 找出做过目标事件e4的人
create view dtl
as
select guid, eventid, ts
from dwd.test_mall_app_evt_dtl
where dt = '2021-09-26' and eventid in ('e1','e2','e3','e4');


-- 2. 对步骤1留下的数据，筛选待归因事件
drop view if exists events;
create view events
as
select
  dtl.guid,eventid,ts
from dtl
join
(
    select
           guid,max(ts) as max_dest_ev_ts
    from dtl
    where eventid = 'e4'
    group by guid
) o
on dtl.guid = o.guid
where ts <= max_dest_ev_ts
;

/*
+--------------+-----------------+------------+
| events.guid  | events.eventid  | events.ts  |
+--------------+-----------------+------------+
| 1            | e1              | 1          |
| 1            | e2              | 2          |
| 1            | e3              | 3          |
| 1            | e3              | 4          |
| 1            | e4              | 5          |
| 1            | e2              | 7          |
| 1            | e4              | 9          |
| 2            | e3              | 1          |
| 2            | e4              | 2          |
| 2            | e1              | 3          |
| 3            | e3              | 1          |
| 3            | e4              | 2          |
| 3            | e1              | 3          |
| 3            | e3              | 5          |
| 3            | e4              | 6          |
+--------------+-----------------+------------+
*/
-- 3.对每一个“待归因-目标”序列求最早的待归因
with tmp as (
    select guid,
      split(regexp_replace(concat_ws(',', sort_array(collect_list(concat_ws(':', cast(ts as string), eventid)))),
      '\\d+:', ''), 'e4') as evseq
    from events
    group by guid
)
/*
+-------+-----------------------+
| guid  |        evseq          |
+-------+-----------------------+
| 1     | e1,e2,e3,e3,e4,e2,e4  |
| 2     | e3,e4,e1              |
| 3     | e3,e4,e1,e3,e4,e4,e4  |
+-------+-----------------------+
+-------+----------------------------+
| guid  |            evseq           |
+-------+----------------------------+
| 1     | ["e1,e2,e3,e3,",",e2,",""] |
| 2     | ["e3," ,",", ","]          |
| 3     | ["e3," ,",e1,e3,",""]      |
+-------+----------------------------+
*/


insert into  table dws.mall_app_evt_attr partition (dt='2021-09-26')
select
  guid
  ,'e4' as dest_ev
  ,'首次触点归因' as strategy
  ,split(regexp_replace(evs,'^,',''),',')[0] as attr_ev
  ,1 as attr_factor
  ,'2021-09-06' as window_start
  ,'2021-09-06' as window_en
from tmp lateral view explode(evseq) o as evs
where evs!='' and evs!=','
/*
+-------+---------------+
| guid  |      evs      |
+-------+---------------+
| 1     | e1,e2,e3,e3,  |
| 1     | e2,           |
| 2     | e3,           |
| 3     | e3,           |
| 3     | e1,e3,        |
+-------+---------------+
*/
