-- 新用户留存人数表
create table dws.mall_app_user_ret(
   first_dt  string,
   retention_days  int,
   retention_user_cnt  bigint,
   dw_date    string
)
    STORED AS ORC
    TBLPROPERTIES('orc.compress'='SNAPPY')
;


-- 测试表：连续活跃区间记录表  test_mall_app_user_ctn_atv
create table dws.test_mall_app_user_ctn_atv(
   guid  bigint,
   first_dt string,
   rng_start string,
   rng_end   string
)
    partitioned by (dt string)
    row format delimited fields terminated by ','
;
-- 连续活跃区间记录表，截止到了2021-09-26号
/*
01,2021-09-01,2021-09-01,2021-09-04
01,2021-09-01,2021-09-06,2021-09-14
01,2021-09-01,2021-09-24,9999-12-31
02,2021-09-01,2021-09-01,2021-09-05
02,2021-09-01,2021-09-08,2021-09-14
03,2021-09-02,2021-09-02,2021-09-06
03,2021-09-02,2021-09-08,2021-09-10
03,2021-09-02,2021-09-19,9999-12-31
04,2021-09-02,2021-09-02,2021-09-05
04,2021-09-02,2021-09-18,2021-09-22
05,2021-09-02,2021-09-02,2021-09-05
05,2021-09-02,2021-09-20,9999-12-31
06,2021-09-24,2021-09-24,9999-12-31
07,2021-09-24,2021-09-24,2021-09-25
08,2021-09-24,2021-09-24,2021-09-25
09,2021-09-24,2021-09-24,9999-12-31
10,2021-09-25,2021-09-25,9999-12-31
11,2021-09-25,2021-09-25,2021-09-25
12,2021-09-25,2021-09-25,2021-09-25
13,2021-09-25,2021-09-25,9999-12-31
14,2021-09-26,2021-09-26,9999-12-31
*/

LOAD DATA LOCAL INPATH '/root/ctn.txt' INTO TABLE dws.test_mall_app_user_ctn_atv PARTITION(dt='2021-09-26');


-- 计算各个日期的新用户留存人数
INSERT INTO TABLE dws.mall_app_user_ret
SELECT
    first_dt,
    datediff('2021-09-26',first_dt) as retention_days,  -- 留存的天数
    count(1)  as retention_user_cnt,
    '2021-09-26' as dw_date
FROM dws.test_mall_app_user_ctn_atv
WHERE dt='2021-09-26' AND rng_end = '9999-12-31'  AND datediff('2021-09-26',first_dt) BETWEEN 1 AND 31
GROUP BY first_dt,datediff('2021-09-26',first_dt)
;
/*
+-------------+-----------------+---------------------+
|  first_dt   | retention_days  | retention_user_cnt  |
+-------------+-----------------+---------------------+
| 2021-09-01  | 25              | 1                   |
| 2021-09-02  | 24              | 2                   |
| 2021-09-24  | 2               | 2                   |
| 2021-09-25  | 1               | 2                   |
+-------------+-----------------+---------------------+
*/


-- 报表查询示例：
--查询一下 2021-09-15号新用户的  次日留存数，2日留存数，3日留存数， .....  11日留存数

SELECT
    first_dt,
    retention_days,
    retention_user_cnt
FROM dws.mall_app_user_ret
WHERE first_dt='2021-09-15';


-- 最近一个月内，哪5个日期的新用户，次日留存最高
SELECT
    first_dt,
    retention_days,
    retention_user_cnt
FROM dws.mall_app_user_ret
WHERE retention_days=1  AND   datediff('2021-09-26',first_dt)<=30
ORDER BY retention_user_cnt DESC
LIMIT 5
