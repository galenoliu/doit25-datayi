-- 路径分析数据汇总表建表

-- 计算日期	时间窗口start	时间窗口end	guid	路径id
create table dws.mall_app_act_route(
    route_id int,
    window_start string,
    window_end string,
    guid   bigint
)
partitioned by (dt string)
stored as orc
tblproperties ('orc.compress'='SNAPPY')
;


-- 计算sql
-- 做过e1，后面做过e4，中间不能有e2

--1. 过滤
with  tmp as (
    select
      o1.guid,
      o2.id,
      o1.ts
    from
    (select guid,
           eventid,
           ts
    from dwd.test_mall_app_evt_dtl
    where dt = '2021-09-26'
      and eventid in ('e1', 'e2', 'e4') ) o1
    join dim.event_id_rel o2
    on o1.eventid=o2.eventid
)

select
--3.匹配正则
1 as route_id,
'2021-09-26' as window_start,
'2021-09-26' as window_end,
guid
from (
    --2.收集事件
    select guid, concat_ws('', collect_list(cast(id as string))) as evs
      from (
               select guid,
                      id,ts
               from tmp
                   distribute by guid
                   sort by ts
           ) o1
      group by guid
     ) o2
where regexp_extract(evs,'1(?!.*2).*(4)',1)='4'