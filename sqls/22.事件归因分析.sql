-- 归因分析数据汇总表建表
drop table  if exists dws.mall_app_evt_attr;
create table dws.mall_app_evt_attr(
   guid    bigint,
   dest_ev string,
   strategy string,
   attr_ev string,
   attr_factor float,
   window_start string,
   window_end string
)
partitioned by (dt string)
stored as orc
tblproperties ('orc.compress'='SNAPPY')
;


-- 测试用的事件明细表
/*
 +---------+------------+------------------------+-------+-------------+
| t.guid  | t.eventid  |      t.properties      | t.ts  |    t.dt     |
+---------+------------+------------------------+-------+-------------+
| 1       | e1         | {"p1":"v1","p3":"v3"}  | 1     | 2021-09-26  |
| 1       | e2         | {"p1":"v2","p2":"v3"}  | 2     | 2021-09-26  |
| 1       | e3         | {"p2":"v5","p3":"v3"}  | 3     | 2021-09-26  |
| 1       | e3         | {"p2":"v4","p3":"v3"}  | 4     | 2021-09-26  |
| 1       | e4         | {"p6":"v2","p2":"v4"}  | 5     | 2021-09-26  |
| 1       | e5         | {"p8":"v8","p3":"v3"}  | 6     | 2021-09-26  |
| 1       | e2         | {"p2":"v4","p3":"v3"}  | 7     | 2021-09-26  |
| 1       | e5         | {"p6":"v2","p2":"v4"}  | 8     | 2021-09-26  |
| 1       | e4         | {"p8":"v8","p3":"v3"}  | 9     | 2021-09-26  | 
| 2       | e3         | {"p2":"v5","p3":"v3"}  | 1     | 2021-09-26  |
| 2       | e4         | {"p1":"v1","p3":"v3"}  | 2     | 2021-09-26  |
| 2       | e1         | {"p1":"v1","p3":"v3"}  | 3     | 2021-09-26  |
| 2       | e5         | {"p8":"v8","p3":"v3"}  | 4     | 2021-09-26  |
| 3       | e3         | {"p2":"v5","p3":"v3"}  | 1     | 2021-09-26  |
| 3       | e4         | {"p1":"v1","p3":"v3"}  | 2     | 2021-09-26  |
| 3       | e1         | {"p1":"v1","p3":"v3"}  | 3     | 2021-09-26  |
| 3       | e5         | {"p8":"v8","p3":"v3"}  | 4     | 2021-09-26  |
| 3       | e3         | {"p2":"v5","p3":"v3"}  | 5     | 2021-09-26  |
| 3       | e4         | {"p2":"v5","p3":"v3"}  | 6     | 2021-09-26  |
+---------+------------+------------------------+-------+-------------+
 */

-- 归因需求
-- 目标事件： e4
-- 待归因事件： e1  e2  e3
-- 归因策略： 首次触点归因

-- 计算sql
-- 1。 找出做过目标事件e4的人
-- 2. 对步骤1留下的数据，筛选待归因事件，然后取最早的那个事件


+--------------+-----------------+------------+
| events.guid  | events.eventid  | events.ts  |  sum(if(eventid='e4',1,0)) over(rows between unbounded preceding and current row)
+--------------+-----------------+------------+
| 1            | e1              | 1          |  0
| 1            | e2              | 2          |  0
| 1            | e3              | 3          |  0  
| 1            | e3              | 4          |  0
| 1            | e4              | 5          |  1
| 1            | e2              | 7          |  1
| 1            | e3              | 7          |  1
| 1            | e4              | 9          |  2
| 2            | e3              | 1          | 
| 2            | e4              | 2          | 
| 3            | e3              | 1          | 
| 3            | e4              | 2          | 
| 3            | e1              | 3          |
| 3            | e3              | 5          |
| 3            | e4              | 6          |
+--------------+-----------------+------------+


WITH tmp as (
SELECT
guid,eventid,flag,ts
FROM
(
SELECT
 guid,
 eventid,
 SUM(IF(eventid='e4',1,0)) OVER(PARTITION BY guid ORDER BY ts ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  as flag,
 ts
FROM events
) o
WHERE eventid!='e4'
)
SELECT
  guid,eventid
FROM (
SELECT
  guid,eventid,ROW_NUMBER() over(partition by guid,flag order by ts) as rn
FROM tmp
) o
WHERE rn=1

+-------+----------+-------+
| guid  | eventid  | flag  |
+-------+----------+-------+
| 1     | e1       | 0     |
| 1     | e2       | 0     |
| 1     | e3       | 0     |
| 1     | e3       | 0     |
| 1     | e4       | 1     |
| 1     | e2       | 1     |
| 1     | e4       | 2     |
| 2     | e3       | 0     |
| 2     | e4       | 1     |
| 3     | e3       | 0     |
| 3     | e4       | 1     |
| 3     | e1       | 1     |
| 3     | e3       | 1     |
| 3     | e4       | 2     |
+-------+----------+-------+


+-------+----------+-------+
| guid  | eventid  | flag  |
+-------+----------+-------+
| 1     | e1       | 0     |
| 1     | e2       | 0     |
| 1     | e3       | 0     |
| 1     | e3       | 0     |
| 1     | e2       | 1     |
| 2     | e3       | 0     |
| 3     | e3       | 0     |
| 3     | e1       | 1     |
| 3     | e3       | 1     |
+-------+----------+-------+


+-------+----------+
| guid  | eventid  |
+-------+----------+
| 1     | e1       |
| 1     | e2       |
| 2     | e3       |
| 3     | e3       |
| 3     | e1       |
+-------+----------+






