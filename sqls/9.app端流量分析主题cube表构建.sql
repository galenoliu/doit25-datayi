-- app端流量分析主题cube表构建
create table dws.mall_app_tfc_cube
(
    appid          string, -- appid
    appversion     string, -- app的版本
    releasechannel string, -- 发布渠道
    carrier        string, -- 运营商
    nettype        string, -- 联网类型
    device_type    string, -- 设备型号
    osname         string, -- 操作系统
    osversion      string, -- 操作系统版本
    resolution     string, -- 分辨率
    province       string, -- 省
    city           string, -- 市
    region         string, -- 区
    enter_page     string, -- 入口页
    exit_page      string, -- 退出页
    uv_cnt         bigint,-- UV数（去重访客数）
    pv_cnt         bigint,-- PV数（页面浏览量）
    ip_cnt         bigint,-- IP数（来访ip数）
    ses_cnt        bigint,-- 用户访问次数（会话次数）
    jmp_cnt        bigint,-- 访问跳出次数（只访问一个页面就退出的会话）（隐式维度：会话是否是跳出会话）
    visit_time     bigint  -- 访问总时长
)
    partitioned by (dt string)
    stored as orc
    tblproperties ('orc.compress' = 'SNAPPY')
;



-- 由于维度字段太多，用with cube将带来无法承受的运算量
-- 所以，我们跟分析师做了一些沟通，挖掘了她有可能的一些维度分析需求，将维度划分成若干个维度组
-- 维度组合只在 各个维度组内进行组合
insert into table dws.mall_app_tfc_cube partition (dt = '2021-09-18')
select appid,                                                                    -- appid
       appversion,                                                               -- app的版本
       releasechannel,                                                           -- 发布渠道
       carrier,                                                                  -- 运营商
       nettype,                                                                  -- 联网类型
       device_type,                                                              -- 设备型号
       osname,                                                                   -- 操作系统
       osversion,                                                                -- 操作系统版本
       resolution,                                                               -- 分辨率
       province,                                                                 -- 省
       city,                                                                     -- 市
       region,                                                                   -- 区
       enter_page,                                                               -- 入口页
       exit_page,                                                                -- 退出页
       count(distinct guid)                                        as uv_cnt,    -- UV数（去重访客数）
       count(if(event_id in ('pageView', 'productView'), 1, null)) as pv_cnt,    -- PV数（页面浏览量）
       count(distinct ip)                                          as ip_cnt,    -- IP数（来访ip数）
       count(distinct session_id)                                  as ses_cnt,   -- 用户访问次数（会话次数）
       count(distinct if(is_jump = 1, session_id, null))           as jmp_cnt,   -- 访问跳出次数（只访问一个页面就退出的会话）（隐式维度：会话是否是跳出会话）
       sum(page_stay_long)                                         as visit_time -- 用户访问时长
from dws.mall_app_tfc_bgt
where dt = '2021-09-18'
group by appid,          -- appid
         appversion,     -- app的版本
         releasechannel, -- 发布渠道

         carrier,        -- 运营商
         nettype,        -- 联网类型

         device_type,    -- 设备型号
         osname,         -- 操作系统
         osversion,      -- 操作系统版本
         resolution,     -- 分辨率

         province,       -- 省
         city,           -- 市
         region,         -- 区

         enter_page,     -- 入口页
         exit_page       -- 退出页
    grouping sets (
    -- base cuboid
    (appid,appversion,releasechannel,carrier,nettype,device_type,osname,ovservion,resolution,province,city,region,enter_page,exit_page),
    ( appid),
    ( appversion),
    -- levle2 cuboid
    ( appid, appversion),
    ( appid, releasechannel),
    ( carrier),
    ( nettype), ( carrier, nettype),
    ( device_type), ( osname), ( osname, osversion), ( osname, resolution),
    ( province), ( province, city), ( province, city, region),
    ( enter_page), ( exit_page),
    ()
    )