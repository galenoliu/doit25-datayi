-- 建表：空设备guid映射表
CREATE TABLE dws.device_guid_idmp(
  device_id string,
  guid      bigint
)
PARTITIONED BY(dt string)
STORED AS ORC
;


-- 空设备guid映射表计算sql


-- 1. 从T日的绑定评分表中，找出所有空设备
/*
d01,u01,120
d01,null,100
d03,null,200
*/
WITH null_device as (
SELECT
  device_id
FROM dws.mall_app_dev_bind
WHERE dt='2021-09-18'
GROUP BY device_id
HAVING max(account) is NULL
),
pre_idmp as(
SELECT
 device_id,
 guid
FROM dws.device_guid_idmp 
WHERE dt='2021-09-17'
)

-- 2. 用T日的空设备去关联 T-1日的 id映射表
-- 存在guid的空设备，继续沿用原来的guid

INSERT INTO TABLE dws.device_guid_idmp PARTITION(dt='2021-09-18')
-- part1: 能关联上旧guid的数据
SELECT
null_device.device_id  ,
pre_idmp.guid 
FROM  null_device join pre_idmp  ON null_device.device_id=pre_idmp.device_id


UNION ALL

-- part2：关联不上旧guid的数据，需要生成新的guid
-- max_guid=`hive -e "SELECT  nvl(max(guid),10000000) as max from pre_idmp"`
SELECT
  o1.device_id,
  o1.rn+max_pre as guid
FROM 
(
   SELECT
    null_device.device_id,
    row_number() over() as rn
   FROM  null_device left join pre_idmp  ON null_device.device_id=pre_idmp.device_id
   WHERE pre_idmp.guid is NULL
) o1

JOIN

(
  SELECT  nvl(max(guid),10000000) as max_pre from pre_idmp
) o2 


