-- noinspection SqlNoDataSourceInspectionForFile

-- dwd层app端行为日志的加工后事件明细表
DROP TABLE IF EXISTS dwd.mall_app_evt_dtl;
CREATE TABLE dwd.mall_app_evt_dtl(
account            string ,
appid              string ,
appversion         string ,
carrier            string ,
device_id          string ,
devicetype         string ,
eventid            string ,
ip                 string ,
latitude           double ,
longitude          double ,
nettype            string ,
osname             string ,
osversion          string ,
properties         string ,
releasechannel     string ,
resolution         string ,
sessionid          string ,
ts                 bigint ,
splitedSessionId   string ,
prvoince           string ,
city               string ,
region             string ,
guid               bigint 
)
PARTITIONED BY (dt string)
STORED AS ORC
TBLPROPERTIES(
  'orc.compress'='snappy'
)

-- 计算逻辑： 是用spark程序做的
