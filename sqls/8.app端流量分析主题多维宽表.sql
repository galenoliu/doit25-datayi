-- 流量分析主题多维宽表(汇总表？)聚合？
DROP TABLE IF EXISTS dws.mall_app_tfc_bgt;
CREATE TABLE dws.mall_app_tfc_bgt
(
    account           string,
    appid             string,
    appversion        string,
    carrier           string,
    device_id         string,
    device_type       string,
    event_id          string,
    ip                string,
    latitude          double,
    longitude         double,
    nettype           string,
    osname            string,
    osversion         string,
    properties        map<string,string>,
    releasechannel    string,
    resolution        string,
    session_id        string,
    ts                bigint,
    splited_SessionId string,
    province          string,
    city              string,
    region            string,
    guid              bigint,
    ses_start_time    bigint,
    ses_end_time      bigint,
    pv_cnt            bigint,
    enter_page        string,
    exit_page         string,
    is_jump           int   ,
    page_stay_long    bigint
)
    PARTITIONED BY (dt string)
    STORED AS ORC
    TBLPROPERTIES ('orc.compress' = 'SNAPPY')
;

-- 计算sql

-- join后的数据
-- g01,s01,v1,t1,se01,se12
-- g01,s01,e2,t2,se01,se12
-- g01,s01,v2,t3,se01,se12
-- g01,s01,e1,t4,se01,se12
-- g01,s01,e2,t5,se01,se12
-- g02,s02,v3,t1,se03,se14
-- g02,s02,e2,t2,se03,se14
-- g02,s02,v4,t3,se03,se14
-- g02,s02,e1,t4,se03,se14
-- g02,s02,e2,t5,se03,se14

-- 过滤出所有的页面浏览事件，并计算页面停留时长
-- g01,s01,v1,t1,se01,se12,100
-- g01,s01,v2,t3,se01,se12,200
-- g02,s02,v3,t1,se03,se14,100
-- g02,s02,v4,t3,se03,se14,300

--过滤出所有非页面浏览时间，将页面停留时长设置为0
-- g01,s01,e2,t2,se01,se12,0
-- g01,s01,e1,t4,se01,se12,0
-- g01,s01,e2,t5,se01,se12,0
-- g02,s02,e2,t2,se03,se14,0
-- g02,s02,e1,t4,se03,se14,0
-- g02,s02,e2,t5,se03,se14,0


-- 然后union all两部分数据
-- g01,s01,v1,t1,se01,se12,100
-- g01,s01,v2,t3,se01,se12,200
-- g02,s02,v3,t1,se03,se14,100
-- g02,s02,v4,t3,se03,se14,300
-- g01,s01,e2,t2,se01,se12,0
-- g01,s01,e1,t4,se01,se12,0
-- g01,s01,e2,t5,se01,se12,0
-- g02,s02,e2,t2,se03,se14,0
-- g02,s02,e1,t4,se03,se14,0
-- g02,s02,e2,t5,se03,se14,0


-- 维度薪资的粒度最好要跟中心事实表的粒度统一
WITH tmp AS (
    SELECT dtl.account
           ,dtl.appid
           ,dtl.appversion
           ,dtl.carrier
           ,dtl.device_id
           ,dtl.device_type
           ,dtl.event_id
           ,dtl.ip
           ,dtl.latitude
           ,dtl.longitude
           ,dtl.nettype
           ,dtl.osname
           ,dtl.osversion
           ,dtl.properties
           ,dtl.releasechannel
           ,dtl.resolution
           ,dtl.session_id
           ,dtl.ts
           ,dtl.splited_SessionId
           ,dtl.province
           ,dtl.city
           ,dtl.region
           ,dtl.guid
           ,ses.start_time as ses_start_time
           ,ses.end_time as ses_end_time
           ,ses.pv_cnt
           ,ses.enter_page
           ,ses.exit_page
           ,ses.is_jump
    FROM (SELECT *
          FROM dwd.mall_app_evt_dtl
          WHERE dt = '2021-09-18') dtl
             LEFT JOIN
         (
             SELECT *
             FROM dws.mall_app_ses_agr
             WHERE dt = '2021-09-18'
         ) ses
         ON dtl.session_id = ses.session_id
)
INSERT INTO TABLE dws.mall_app_tfc_bgt PARTITION (dt='2021-09-18')
SELECT account
       ,appid
       ,appversion
       ,carrier
       ,device_id
       ,device_type
       ,event_id
       ,ip
       ,latitude
       ,longitude
       ,nettype
       ,osname
       ,osversion
       ,properties
       ,releasechannel
       ,resolution
       ,session_id
       ,ts
       ,splited_SessionId
       ,province
       ,city
       ,region
       ,guid
       ,ses_start_time
       ,ses_end_time
       ,pv_cnt
       ,enter_page
       ,exit_page
       ,is_jump
       ,lead(ts,1,ses_end_time) over(partition by guid,session_id order by ts) -ts as page_stay_long
FROM tmp
WHERE event_id IN ('pageView','productView')

UNION ALL

SELECT account
       ,appid
       ,appversion
       ,carrier
       ,device_id
       ,device_type
       ,event_id
       ,ip
       ,latitude
       ,longitude
       ,nettype
       ,osname
       ,osversion
       ,properties
       ,releasechannel
       ,resolution
       ,session_id
       ,ts
       ,splited_SessionId
       ,province
       ,city
       ,region
       ,guid
       ,ses_start_time
       ,ses_end_time
       ,pv_cnt
       ,enter_page
       ,exit_page
       ,is_jump
       ,0 as page_stay_long
FROM tmp
WHERE event_id NOT IN ('pageView','productView')