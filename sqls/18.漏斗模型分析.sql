-- 漏斗模型分析中间汇总表
CREATE  TABLE dws.mall_app_fun(
                                  guid           string,   -- 用户标识
                                  funnel_name    string,   -- 漏斗名称
                                  window_start   string,   -- 统计窗口起始日期
                                  window_end     string,   -- 统计窗口结束日期
                                  max_step       int       -- 完成的最大步骤
)
    PARTITIONED BY (dt string)
    STORED AS ORC
    TBLPROPERTIES('orc.compress'='SNAPPY')
;




-- 漏斗模型举例：
--1. 看到首页上的“海澜之家”闪购运营位图片
--2. 点击“海澜之家”闪购运营位图片进入闪购活动页
--3. 点击闪购活动页的“海澜之家”闪购商品页
--4. 在“海澜之家”闪购商品页上点了商品详情
--5. 添加了闪购商品到购物车
--6. 下单支付


--计算，给定漏斗模型中，每一个步骤上的完成人数

/*
1. 10000
2. 5000
3. 50
4. 5
5. 1
6. 0
*/

-- 需求的抽象
--1. eventid='adShow' & properties['adId']=6
--2. eventid='adClick' & properties['adId']=6
--3. eventid='adClick' & properties['adId']=18 & properties['url']='/red.jd.com/?brandId=220832'
--4. eventid='productClick' & properties['url']='/red.jd.com/goodsList?brandId=220832'
--5. eventid='addCart' & properties['pd_categoryid']='220832'

-- 学习抽象
--1.eventid='e1' & properties['p1']='v1'
--2.eventid='e3' & properties['p2']='v5'
--3.eventid='e4'
--.....


-- 有了学习抽象，测试这个套路就变得很简洁了

-- 抽象后的漏斗模型：
--1.eventid='e1' & properties['p1']='v1'
--2.eventid='e3' & properties['p2']='v5'
--3.eventid='e4'
--4.eventid='e5' & properties['p8']='v8'


-- 测试数据 ,对应咱们真实数据的 dwd.mall_app_evt_dtl表
/*
guid,eventid,properties,ts
1,e1,p1:v1_p3:v3,1
1,e2,p1:v2_p2:v3,2
1,e3,p2:v5_p3:v3,3
1,e3,p2:v4_p3:v3,4
1,e4,p6:v2_p2:v4,5
1,e5,p8:v8_p3:v3,6
2,e3,p2:v5_p3:v3,1
2,e4,p1:v1_p3:v3,2
2,e1,p1:v1_p3:v3,3
2,e5,p8:v8_p3:v3,4
3,e3,p2:v5_p3:v3,1
3,e4,p1:v1_p3:v3,2
3,e1,p1:v1_p3:v3,3
3,e5,p8:v8_p3:v3,4
3,e3,p2:v5_p3:v3,5
3,e4,p2:v5_p3:v3,6
*/
-- 事件明细测试表建表
create table dwd.test_mall_app_evt_dtl(
                                          guid  bigint,
                                          eventid  string,
                                          properties map<string,string>,
                                          ts   bigint
)
    partitioned by (dt string)
    row format delimited fields terminated by ','
        collection items terminated by '_'
        map keys terminated by ':'
;

load data local inpath '/root/dtl.txt' into table dwd.test_mall_app_evt_dtl partition(dt='2021-09-26');

-- 漏斗模型中间服务表计算sql
-- 核心逻辑：
--步骤1：过滤出漏斗模型中关心的事件
1,e1,p1:v1_p3:v3,1

1,e3,p2:v5_p3:v3,3

1,e4,p6:v2_p2:v4,5
1,e5,p8:v8_p3:v3,6

2,e3,p2:v5_p3:v3,1
2,e4,p1:v1_p3:v3,2
2,e1,p1:v1_p3:v3,3
2,e5,p8:v8_p3:v3,4

3,e3,p2:v5_p3:v3,1
3,e4,p1:v1_p3:v3,2
3,e1,p1:v1_p3:v3,3
3,e5,p8:v8_p3:v3,4
3,e3,p2:v5_p3:v3,5
3,e4,p2:v5_p3:v3,6

-- 步骤2：留下满足条件的guid和eventid和ts即可
1,e1,1
1,e3,3
1,e4,5
1,e5,6
2,e3,1
2,e4,2
2,e1,3
2,e5,4
3,e3,1
3,e4,2
3,e1,3
3,e5,4
3,e3,5
3,e4,6


-- 步骤3：将每个用户的事件名称收集成一个数组，并按时间有序
1,"1_e1,3_e3,5_e4,6_e5"
2,"1_e3,2_e4,3_e1,4_e5"
3,"1_e3,2_e4,3_e1,4_e5,5_e3,6_e4"



-- 步骤4：利用正则匹配来计算每个人实现的最大步骤号


-- 完整sql： ------------------------------------
WITH tmp as (
    SELECT guid
         ,concat_ws(',',sort_array(collect_list(concat_ws('_',ts || '',eventid)))) as events
    FROM dwd.test_mall_app_evt_dtl
    WHERE dt BETWEEN '2021-09-24' AND  '2021-09-26'
      AND  (( eventid='e1' and properties['p1']='v1' )
        OR
            ( eventid='e3' and properties['p2']='v5'  )
        OR
            ( eventid='e4')
        OR
            ( eventid='e5' and properties['p8']='v8'  ))
    GROUP BY  guid
)
/*
+-------+--------------------------------+
| guid  |             events             |
+-------+--------------------------------+
| 1     | 1_e1,3_e3,5_e4,6_e5            |
| 2     | 1_e3,2_e4,3_e1,4_e5            |
| 3     | 1_e3,2_e4,3_e1,4_e5,5_e3,6_e4  |
+-------+--------------------------------+
*/
INSERT INTO TABLE dws.mall_app_fun PARTITION(dt='2021-09-26')
SELECT
    guid,
    '海家闪购'  as funnel_name,
    '2021-09-24' as window_start,
    '2021-09-26' as window_end,
    case
        when regexp_extract(events,'.*?(e1).*?(e3).*?(e4).*?(e5).*?',4)='e5' then 4
        when regexp_extract(events,'.*?(e1).*?(e3).*?(e4).*?',3)='e4' then 3
        when regexp_extract(events,'.*?(e1).*?(e3).*?',2)='e3' then 2
        when regexp_extract(events,'.*?(e1).*?',1)='e1' then 1
        else 0
        end as max_step
FROM tmp


-- 报表查询示例：
--漏斗名称,步骤,人数,时间窗口,             计算日期
--'海家闪购',1,100000,[2021-09-24,2021-09-26],2021-09-26
--'海家闪购',2,8000,  [2021-09-24,2021-09-26],2021-09-26
--'海家闪购',3,200,   [2021-09-24,2021-09-26],2021-09-26
--'海家闪购',4,50,    [2021-09-24,2021-09-26],2021-09-26

-- 方案1，用4个表达式分别计算4个步骤上的人数
SELECT
    sum(if(max_step>=1,1,0)) as step_1,
    sum(if(max_step>=2,1,0)) as step_2,
    sum(if(max_step>=3,1,0)) as step_3,
    sum(if(max_step>=4,1,0)) as step_4
FROM dws.mall_app_fun
WHERE dt='2021-09-26'
  AND funnel_name='海家闪购'
  AND window_start='2021-09-24'
  AND window_end='2021-09-26'
;

-- 方案2，略有瑕疵，累加逻辑是可以
/*
4,5     ->  5
3,10    ->  15
2,50    ->  65
1,100   ->  165
*/

-- 但是如果数据中缺失某个步骤，则结果中也不会出现该步骤
with tmp as (
    select max_step,
           count(*) as total
    from dws.mall_app_fun
    where dt = '2021-09-26'
      and funnel_name = '海家闪购'
      and window_start = '2021-09-24'
      and window_end = '2021-09-26'
    group by max_step
    order by max_step desc
)
select
    max_step,
    sum(total) over (order by max_step desc rows between unbounded preceding and current row)
from tmp;


-- 方案3：把一行变多行
/*
1,3 =>    1,1  ;  1,2  ; 1,3
2,2 =>    2,1  ;  2,2
3,4
4,2
5,3
*/
WITH tmp as (
-- 拼接数组array
    SELECT
        guid,
        case
            when max_step=4  then array(concat_ws(',',cast(guid as string),'4'),concat_ws(',',cast(guid as string),'3'),concat_ws(',',cast(guid as string),'2'),concat_ws(',',cast(guid as string),'1'))
            when max_step=3  then array(concat_ws(',',cast(guid as string),'3'),concat_ws(',',cast(guid as string),'2'),concat_ws(',',cast(guid as string),'1'))
            when max_step=2  then array(concat_ws(',',cast(guid as string),'2'),concat_ws(',',cast(guid as string),'1'))
            when max_step=1  then array(concat_ws(',',cast(guid as string),'1'))
            else array(concat_ws(',',cast(guid as string),'0'))
            end as arr
    FROM dws.mall_app_fun
    WHERE dt='2021-09-26'
      AND funnel_name='海家闪购'
      AND window_start='2021-09-24'
      AND window_end='2021-09-26'
)
SELECT
    step,
    count(1) as step_uses
FROM (
         SELECT
             guid,
             split(step,',')[1] as step
         FROM tmp  lateral view explode(arr) o as step
     ) o1
GROUP BY step

/*
+-------+------------+
| step  | step_uses  |
+-------+------------+
| 1     | 3          |
| 2     | 2          |
| 3     | 2          |
| 4     | 1          |
+-------+------------+
*/

-- 拼接hashmap
SELECT
    guid,
    case
        when max_step=4  then str_to_map(guid || ':4,'|| guid ||  ':3,'|| guid ||  ':2,'|| guid ||  ':1')
        when max_step=3  then str_to_map(guid ||  ':3,'|| guid ||  ':2,'|| guid ||  ':1')
        when max_step=2  then str_to_map(guid ||  ':2,'|| guid ||  ':1')
        when max_step=1  then str_to_map(guid ||  ':1')
        else str_to_map(guid ||  ':0')
        end as arr
FROM dws.mall_app_fun
WHERE dt='2021-09-26'
  AND funnel_name='海家闪购'
  AND window_start='2021-09-24'
  AND window_end='2021-09-26'
;