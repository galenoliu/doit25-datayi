#!/bin/bash

#
#  @auther: deep as the sea
#  @qq: 83544844
#  @wx: angelababy
#  @date: 2021-09-19
#  @des: 商城app行为日志加载入库
#
#

export HIVE_HOME=/opt/apps/hive

dt=$(date -d'-1 day' +%Y-%M-%d)

if [ $# -gt 0 ];then
dt=$1
fi

echo "INFO:  loading /logdata/applog/${dt} to  ods.mall_app_action_log partition(dt='${dt}') "

${HIVE_HOME}/bin/hive -e "load data inpath '/logdata/applog/${dt}' into table ods.mall_app_action_log partition(dt='${dt}')"

if [ $? -eq 0 ];then
echo "dw task sucessed :load data: applog/${dt} to ods.mall_app_action_log partition(dt='${dt}')" | mail -s '数易平台任务运行成功通知' 83544844@qq.com
else
echo "dw task failed :load data: applog/${dt} to ods.mall_app_action_log partition(dt='${dt}')" | mail -s '数易平台任务运行失败通知' 83544844@qq.com
fi
