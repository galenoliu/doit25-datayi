#!/bin/bash

#
#  @auther: deep as the sea
#  @qq: 83544844
#  @wx: doit_edu
#  @date: 2021-09-21
#  @des: 设备账号最终绑定表计算任务
#
#

export HIVE_HOME=/opt/apps/hive

dt=`date -d'-1 day' +%Y-%m-%d`


if [ $# -gt 0 ];then
dt=$1
fi


echo "INFO:  设备账号最终绑定表计算任务开始，评分表(${dt})  => 最终账号绑定表(${dt})"

${HIVE_HOME}/bin/hive -e "
INSERT INTO TABLE dws.mall_app_dev_bind_f PARTITION(dt='${dt}')
SELECT
 device_id,
 account
FROM
(
SELECT
  device_id,
  account,
  row_number() over(partition by device_id order by bind_score desc,last_login desc ) as rn 
FROM dws.mall_app_dev_bind
WHERE dt='${dt}'
) o 
WHERE rn=1
;


"

if [ $? -eq 0 ];then
echo "dw task sucessed :设备账号最终绑定表计算任务执行成功，评分表(${dt})  => 最终账号绑定表(${dt})" | mail -s '数易平台任务运行成功通知' 83544844@qq.com
else
echo "dw task failed :设备账号最终绑定表计算任务执行失败，评分表(${dt})  => 最终账号绑定表(${dt})" | mail -s '数易平台任务运行失败通知' 83544844@qq.com
fi
